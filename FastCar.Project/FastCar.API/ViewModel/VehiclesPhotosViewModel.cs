﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.ViewModel
{
    public class VehiclesPhotosViewModel
    {
        public int Id { get; set; }

        public int Order { get; set; }

        public string Url { get; set; }

        public int SaleVehicleId { get; set; }
    }
}
