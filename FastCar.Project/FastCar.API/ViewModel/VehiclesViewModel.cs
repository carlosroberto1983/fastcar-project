﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.ViewModel
{
    public class VehiclesViewModel
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string Name { get; set; }

        public List<VehiclesPhotosViewModel> Photos { get; set; }
        public List<VehiclesOptionalsViewModel> Optionals { get; set; }

    }
}
