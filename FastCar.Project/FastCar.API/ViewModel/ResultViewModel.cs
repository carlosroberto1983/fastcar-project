﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.ViewModel
{
    public class ResultViewModel
    {
        public int Count { get; set; }

        public int NumberOfPages { get; set; }

        public List<RecordsViewModel> Records { get; set; }


    }
}
