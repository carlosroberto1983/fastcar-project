﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.ViewModel
{
    public class LeadViewModel
    {
        public int idLoja { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }

    }
}
