﻿using AutoMapper;
using FastCar.API.ViewModel;
using FastCar.Domain.Interface;
using FastCar.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FastCar.API.V2.Controllers
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("v{version:apiVersion}/lead")]
    public class LeadController : ControllerBase
    {
        private ILeadServices _services;
        IMapper _mapper;
        public LeadController(ILeadServices services, IMapper mapper)
        {
            _services = services;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("gerarlead")]
        public async Task<ActionResult> Add([FromBody] LeadViewModel leadviewmodel)
        {
            try
            {
                //TODO : melhorar a Implementação do tratamento de CPF
                Lead lead = new Lead();
                if (!lead.ValidaCPF(leadviewmodel.CPF))
                    return BadRequest(new { message = "CPF inválido. Considere 11 caracteres..." });

                var requestLead = _mapper.Map<LeadViewModel, Lead>(leadviewmodel);
                var response = await _services.Add(requestLead);

                return Ok(new { response.id, response.cpf, response.success });
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }
    }
}
