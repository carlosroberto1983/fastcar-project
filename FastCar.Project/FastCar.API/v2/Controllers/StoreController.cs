﻿using AutoMapper;
using FastCar.API.ViewModel;
using FastCar.Domain.Interface;
using FastCar.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastCar.API.v2.Controllers
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("v{version:apiVersion}/lojas")]
    public class StoreController : ControllerBase
    {
        private readonly IStoreServices _services;
        IMapper _mapper;
        public StoreController(IStoreServices services, IMapper mapper)
        {
            _services = services;
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {
                var response = _mapper.Map<List<Store>, List<StoreViewModel>>(await _services.GetAll());
                return Ok(response);
            }
            catch (System.Exception)
            {
                return BadRequest("Ocorreu um erro. Entre em contato com adm@adm.com");
            }
        }
    }
}
