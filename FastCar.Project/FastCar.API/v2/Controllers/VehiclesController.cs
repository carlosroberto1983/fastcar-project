﻿using AutoMapper;
using FastCar.API.ViewModel;
using FastCar.Domain.DTO;
using FastCar.Domain.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.V2.Controllers
{
    [ApiController]
    [ApiVersion("1.0", Deprecated = true)]
    [ApiVersion("2.0")]
    [Route("v{version:apiVersion}/veiculos")]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehicleServices _services;
        IMapper _mapper;
        public VehiclesController(IVehicleServices services, IMapper mapper)
        {
            _services = services;
            _mapper = mapper;
        }
        [HttpGet("veiculos")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var response = _mapper.Map<List<VehiclesDTO>, List<VehiclesViewModel>>(await _services.GetAll());
                return Ok(response);
            }
            catch (System.Exception)
            {
                return BadRequest("Ocorreu um erro. Entre em contato com adm@adm.com");
            }
        }
        [HttpGet("store/{storeId}")]
        public async Task<ActionResult> GetByStoreId(int storeId)
        {
            try
            {
                var response = _mapper.Map<List<VehiclesDTO>, List<VehiclesViewModel>>(await _services.GetByStoreId(storeId));
                return Ok(response);
            }
            catch (System.Exception)
            {
                return BadRequest("Ocorreu um erro. Entre em contato com adm@adm.com");
            }
        }
        [HttpGet("{vehicleid}")]
        public async Task<ActionResult> GetById(int vehicleid)
        {
            try
            {
                var response = _mapper.Map<VehiclesDTO, VehiclesViewModel>(await _services.GetById(vehicleid));
                return Ok(response);
            }
            catch (System.Exception)
            {
                return BadRequest("Ocorreu um erro. Entre em contato com adm@adm.com");
            }
        }

    }
}
