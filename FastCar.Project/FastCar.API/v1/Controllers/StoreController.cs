﻿using FastCar.Domain.Interface;
using Microsoft.AspNetCore.Mvc;

namespace FastCar.API.V1.Controllers
{
    [ApiController]
    [ApiVersion("1.0", Deprecated = true)]
    [Route("v{version:apiVersion}/lojas")]
    public class StoreController : ControllerBase
    {
        private readonly IStoreServices _repository;
        public StoreController(IStoreServices repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var response = _repository.GetAll();
                return Ok(response);
            }
            catch (System.Exception)
            {
                return BadRequest("Ocorreu um erro. Entre em contato com adm@adm.com");
            }
        }
    }
}
