﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastCar.API.Controllers.V3
{
    [ApiController]
    [ApiVersion("3.0")]
    [Route("v{version:apiVersion}/lead")]
    public class LeadController : ControllerBase
    {
        public LeadController()
        {

        }
        [HttpPost]
        [Route("add")]
        public ActionResult Add()
        {
            return Ok("Hello from Lead V3");
        }
    }
}
