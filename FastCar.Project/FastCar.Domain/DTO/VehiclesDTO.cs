﻿using FastCar.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastCar.Domain.DTO
{
    public class VehiclesDTO
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string Name { get; set; }
        public List<Optionals> Optionals { get; set; }
        public List<Photos> Photos { get; set; }

    }
}
