﻿using FastCar.Domain.Model;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace FastCar.Domain.DTO
{
    public class RecordsDTO
    {
        
        public int Count { get; set; }
        
        public int NumberOfPages { get; set; }
        
        public List<Records> Records { get; set; }
        
    }
}
