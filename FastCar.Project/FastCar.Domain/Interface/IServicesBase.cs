﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastCar.Domain.Interface
{
    public interface IServicesBase<T> where T : class
    {
        Task<List<T>> GetAll();
        Task<T> Get();
        Task<T> GetById(int id);
    }
}
