﻿using FastCar.Domain.DTO;
using FastCar.Domain.Model;
using System.Threading.Tasks;

namespace FastCar.Domain.Interface
{
    public interface ILeadServices
    {
        Task<LeadDTO> Add(Lead lead);
    }
}
