﻿using FastCar.Domain.DTO;
using FastCar.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastCar.Domain.Interface
{
    public interface IVehicleServices : IServicesBase<VehiclesDTO>
    {
        Task<List<VehiclesDTO>> GetByStoreId(int IdCarDealer);
    }
}
