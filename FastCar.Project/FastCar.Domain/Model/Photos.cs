﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastCar.Domain.Model
{
    public class Photos
    {
        public int Id { get; set; }

        public int Order { get; set; }

        public string Url { get; set; }

        public int SaleVehicleId { get; set; }
    }
}
