﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastCar.Domain.Model
{
    public class Lead
    {
        public int idLoja { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }

        public bool ValidaCPF(string CPF)
        {
            bool isValid = CPF.Trim().Length == 11 ? true : false;
            return isValid;
        }
    }
}
