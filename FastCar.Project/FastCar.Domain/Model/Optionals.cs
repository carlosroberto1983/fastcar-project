﻿using Newtonsoft.Json;

namespace FastCar.Domain.Model
{
    public class Optionals
    {
        
        public int Id { get; set; }
        
        public string Description { get; set; }
    }
}
