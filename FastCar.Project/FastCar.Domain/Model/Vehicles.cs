﻿using Newtonsoft.Json;

namespace FastCar.Domain.Model
{
    public class Vehicles
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public string Name  { get; set; }
    }
}
