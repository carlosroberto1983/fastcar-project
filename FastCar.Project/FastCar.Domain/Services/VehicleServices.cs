﻿using FastCar.Domain.DTO;
using FastCar.Domain.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FastCar.Domain.Services
{
    public class VehicleServices : IVehicleServices
    {

        const string url = "https://demo5674440.mockable.io";
        public async Task<VehiclesDTO> Get()
        {
            try
            {
                VehiclesDTO vehicleDTO = new VehiclesDTO();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync($"{url}/veiculos"))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var vehiclesAPI = await response.Content.ReadAsStringAsync();
                            var vehicles = JsonConvert.DeserializeObject<RecordsDTO>(vehiclesAPI);
                            foreach (var item in vehicles.Records)
                            {
                                vehicleDTO.Id = item.Id;
                                vehicleDTO.StoreId = item.StoreId;
                                vehicleDTO.Name = item.Name;
                                vehicleDTO.Optionals = item.Optionals;
                                vehicleDTO.Photos = item.Photos;
                            }
                        }
                    }
                }
                return vehicleDTO;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<VehiclesDTO>> GetAll()
        {
            try
            {
                List<VehiclesDTO> vehicles = new List<VehiclesDTO>();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync($"{url}/veiculos"))
                    {
                        var vehiclesAPI = await response.Content.ReadAsStringAsync();
                        var vehiclesList = JsonConvert.DeserializeObject<RecordsDTO>(vehiclesAPI);
                        foreach (var item in vehiclesList.Records)
                        {
                            vehicles.Add(new VehiclesDTO()
                            {
                                Id = item.Id,
                                StoreId = item.StoreId,
                                Name = item.Name,
                                Optionals = item.Optionals,
                                Photos = item.Photos
                            });
                        }
                    }
                }
                return vehicles;
            }
            catch (Exception)
            {

                return null;
            }

        }

        public async Task<List<VehiclesDTO>> GetByStoreId(int storeId)
        {
            try
            {
               
                List<VehiclesDTO> v = new List<VehiclesDTO>();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync($"{url}/veiculos"))
                    {
                        var vehiclesAPI = await response.Content.ReadAsStringAsync();
                        var vehicles = JsonConvert.DeserializeObject<RecordsDTO>(vehiclesAPI);
                        var v1 = vehicles.Records.Where(w => w.StoreId == storeId);

                        foreach (var item in v1)
                        {
                            v.Add(new()
                            {
                                Id = item.Id,
                                StoreId = item.StoreId,
                                Name = item.Name,
                                Optionals = item.Optionals,
                                Photos = item.Photos,
                            });
                        }
                    }
                }


                return v;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<VehiclesDTO> GetById(int id)
        {
            try
            {
                VehiclesDTO vehicle = new VehiclesDTO();
                var request = await RequestApi("veiculos");
                var vehicles = JsonConvert.DeserializeObject<RecordsDTO>(request);
                var filter = vehicles.Records.Where(w => w.Id == id);

                foreach (var item in filter)
                {
                    vehicle.Id = item.Id;
                    vehicle.StoreId = item.StoreId;
                    vehicle.Name = item.Name;
                    vehicle.Optionals = item.Optionals;
                    vehicle.Photos = item.Photos;
                }
                return vehicle;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

        private async Task<string> RequestApi(string endpoint)
        {
            try
            {
                var response = String.Empty;
                using (var httpClient = new HttpClient())
                {
                    using (var client = await httpClient.GetAsync($"{url}/{endpoint}"))
                    {
                        if (client.IsSuccessStatusCode)
                        {
                            response = await client.Content.ReadAsStringAsync();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
