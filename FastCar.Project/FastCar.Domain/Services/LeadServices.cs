﻿using FastCar.Domain.DTO;
using FastCar.Domain.Interface;
using FastCar.Domain.Model;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FastCar.Domain.Services
{
    public class LeadServices : ILeadServices
    {
        const string url = "https://demo5674440.mockable.io";
        public async Task<LeadDTO> Add(Lead leadValue)
        {

            try
            {
                if (!leadValue.ValidaCPF(leadValue.CPF))
                    return null;

                LeadDTO leadDTO = new LeadDTO();
                using (var httpClient = new HttpClient())
                {
                    string contents = JsonSerializer.Serialize(leadValue);
                    var data = new System.Net.Http.StringContent(contents, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync($"{url}/gerarLead", data))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var leadResult = response.Content.ReadAsStringAsync().Result;
                            leadDTO = JsonSerializer.Deserialize<LeadDTO>(leadResult);
                            return leadDTO;
                        }
                    }
                }
                return leadDTO;
            }
            catch (System.Exception)
            {
                return null;
            }

        }
    }
}
