﻿using FastCar.Domain.Interface;
using FastCar.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace FastCar.Domain.Services
{
    public class StoreServices : IStoreServices
    {
        const string url = "https://demo5674440.mockable.io";

        public Task<Store> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<List<Store>> GetAll()
        {
            try
            {
                List<Store> carDealers = new List<Store>();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync($"{url}/lojas"))
                    {
                        string carDealersAPI = await response.Content.ReadAsStringAsync();
                        carDealers = JsonSerializer.Deserialize<List<Store>>(carDealersAPI);
                    }
                }
                carDealers = carDealers.Where(w => w.active).ToList();
                return carDealers;
            }
            catch (Exception)
            {

                return null;
            }
            
        }
        public Task<Store> GetById(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<List<Store>> GetCarDealers()
        {
            List<Store> carDealers = new List<Store>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync($"{url}/lojas"))
                {
                    string carDealersAPI = await response.Content.ReadAsStringAsync();
                    carDealers = JsonSerializer.Deserialize<List<Store>>(carDealersAPI);
                }
            }
            return carDealers;
        }
    }
}
