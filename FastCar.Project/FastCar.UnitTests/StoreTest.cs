﻿using FastCar.Domain.Services;
using Xunit;

namespace FastCar.UnitTests
{
    public class StoreTest
    {
        private readonly StoreServices services = new StoreServices();

        [Fact]
        public void GetStores()
        {
            bool isCalled = services.GetAll().Result != null;
            Assert.True(isCalled);
        }
    }
}
