﻿using FastCar.Domain.Model;
using FastCar.Domain.Services;
using Xunit;

namespace FastCar.UnitTests
{
    public class LeadTest
    {
        private readonly LeadServices services = new LeadServices();

        [Fact]
        public void GetStores()
        {

            Lead lead = new Lead { CPF = "12345678901", idLoja = 1, Nome = "Chico Xavier" };

            bool isCalled = services.Add(lead).Result != null;
            Assert.True(isCalled);
        }
    }
}
