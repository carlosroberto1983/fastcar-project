﻿using FastCar.Domain.Services;
using Xunit;

namespace FastCar.UnitTests
{
    public class VehicleTest
    {
        private readonly VehicleServices services = new VehicleServices();

        [Fact]
        public void GetVehicles()
        {
            bool isCalled = services.GetAll().Result != null;
            Assert.True(isCalled);
        }
        [Fact]
        public void GetVehicleById()
        {
            bool isCalled = services.GetById(2645).Result != null;
            Assert.True(isCalled);
        }
        [Fact]
        public void GetVehicleByStoreId()
        {
            bool isCalled = services.GetByStoreId(6).Result != null;
            Assert.True(isCalled);
        }
    }
}
