# README #

Este README possui as instruções para execução e entendimento do Rapidex APP

### Qual a finalidade deste repositório ? ###

* Rapidex é um app que consome informações dos endpoints da Fastcar.
* Versão 1.0

### Cloning ###

* O clone pode ser realizado com o comando: git clone https://carlosroberto1983@bitbucket.org/carlosroberto1983/fastcar-project.git

### Swagger ###

* V1 Deprecated
* V2 Current
* V3 Lab

### Instalação e Dependências ###

* Baixar o SDK e Runtime do netCore5 em https://dotnet.microsoft.com/download/dotnet/5.0
* Dependências:
	AutoMapper (8.0.0), 
	Xunit	(2.4.1), 
	Newtonsoft (13.0.1)
* Os testes podem ser executados em FastCar.UnitTests

### Running ###

* Executar o arquivo run.bat na pasta raiz da aplicação
* Este App está configurado como http://localhost:2000
* Considerar a seguinte url: http://localhost:2000/swagger

### Staging ###

* Foi disponibilizado um ambiente staging em: https://fastcar.giancristoforo.com.br/swagger

### Documentos Auxiliares ###

* Este software está sob avaliação com base no documento Avaliação.docx em FastCar.Configuration

### Postman ###

* Fazer o download em https://www.postman.com/downloads/
* Fazer a importação das navegações: arquivo Rapidex API.postman_collection.json no diretório FastCar.Configuration
* Fazer a importação dos environments: arquivos Rapidex Staging.postman_environment.json e Development.postman_environment.json também no diretório FastCar.Configuration
